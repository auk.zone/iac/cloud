import click
import os
import logging
import sys
from . import __version__

plugin_folder = os.path.join(os.path.dirname(__file__), 'commands')


class AukCli(click.MultiCommand):

    def list_commands(self, ctx):
        rv = []
        for filename in os.listdir(plugin_folder):
            if filename.endswith('.py') and filename != '__init__.py':
                rv.append(filename[:-3])
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        ns = {}
        fn = os.path.join(plugin_folder, name + '.py')
        with open(fn) as f:
            code = compile(f.read(), fn, 'exec')
            eval(code, ns, ns)
        return ns['auk']


@click.command(cls=AukCli, help="Auk is command line tool to help you manage your infrastructure")
@click.version_option(version=__version__)
def main():
    logging.basicConfig(level=logging.INFO)


if __name__ == '__main__':
    sys.exit(main())
