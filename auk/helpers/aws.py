import os.path

from aws_cdk import Stack, App
from aws_cdk import aws_ec2 as ec2
from aws_cdk import aws_iam as iam
from aws_cdk.aws_s3_assets import Asset
from constructs import Construct
from aws_cdk.cli_lib_alpha import ICloudAssemblyDirectoryProducer
import boto3
import jsii

dirname = os.path.dirname(__file__)


class AWSApiGateway():
    def __init__(self) -> None:
        super().__init__

    def create(self):
        apigateway_client = boto3.client('apigateway')

        api_name = 'MyAPI'
        api_description = 'My API Gateway'
        api_stage_name = 'dev'

        api_response = apigateway_client.create_rest_api(
            name=api_name,
            description=api_description
        )

        api_id = api_response['id']

        lambda_functions = [
            {'name': 'MyLambda1', 'path': 'my-lambda-1'},
            {'name': 'MyLambda2', 'path': 'my-lambda-2'},
            {'name': 'MyLambda3', 'path': 'my-lambda-3'},
            {'name': 'MyLambda4', 'path': 'my-lambda-4'},
            {'name': 'MyLambda5', 'path': 'my-lambda-5'}
        ]

        for function in lambda_functions:
            # Create API Gateway resource
            resource_response = apigateway_client.create_resource(
                restApiId=api_id,
                parentId='your-parent-resource-id',  # Replace with the parent resource ID
                pathPart=function['path']
            )

            resource_id = resource_response['id']

            # Create API Gateway method
            method_response = apigateway_client.put_method(
                restApiId=api_id,
                resourceId=resource_id,
                httpMethod='POST',
                authorizationType='NONE'
            )

            # Get the ARN of the Lambda function
            lambda_arn = boto3.client('lambda').get_function(
                FunctionName=function['name']
            )['Configuration']['FunctionArn']

            # Create API Gateway integration with Lambda
            integration_response = apigateway_client.put_integration(
                restApiId=api_id,
                resourceId=resource_id,
                httpMethod='POST',
                type='AWS',
                integrationHttpMethod='POST',
                uri=lambda_arn
            )

            # Create API Gateway method response
            apigateway_client.put_method_response(
                restApiId=api_id,
                resourceId=resource_id,
                httpMethod='POST',
                statusCode='200'
            )

            # Create API Gateway integration response
            apigateway_client.put_integration_response(
                restApiId=api_id,
                resourceId=resource_id,
                httpMethod='POST',
                statusCode='200',
                selectionPattern=''
            )

        # Deploy the API Gateway
        deployment_response = apigateway_client.create_deployment(
            restApiId=api_id,
            stageName=api_stage_name
        )

        print(f'API Gateway deployed: {deployment_response["id"]}')


class AWSEC2():

    def __init__(self) -> None:
        super().__init__
        os.environ['AWS_ACCESS_KEY_ID'] = os.getenv('AWS_ACCESS_KEY_ID')
        os.environ['AWS_SECRET_ACCESS_KEY'] = os.getenv(
            'AWS_SECRET_ACCESS_KEY')
        os.environ['AWS_DEFAULT_REGION'] = os.getenv('AWS_DEFAULT_REGION')

    def create(self):
        ec2_client = boto3.client('ec2')

        ec2_response = ec2_client.run_instances(
            ImageId='ami-0715c1897453cabd1',
            InstanceType='t2.micro',
            MinCount=1,
            MaxCount=1
        )

        instance_id = ec2_response['Instances'][0]['InstanceId']

        ec2_waiter = ec2_client.get_waiter('instance_running')
        ec2_waiter.wait(InstanceIds=[instance_id])

        ec2_resource = boto3.resource('ec2', region_name='us-east-1')
        instance = ec2_resource.Instance(instance_id)

        user_data = '''
        #!/bin/bash
        sudo yum update
        sudo yum -y install docker
        sudo systemctl start docker
        sudo docker run -d -p 80:80 nginx
        '''

        instance.modify_attribute(
            UserData={
                'Value': user_data
            }
        )

        instance.wait_until_running()
        instance.load()
        public_ip = instance.public_ip_address
        print(f'EC2 instance is running with public IP: {public_ip}')

    def destroy(self, instance_ids: list = [], all: bool = True):
        ec2 = boto3.resource('ec2')
        if all:
            instance_ids = self.filter()

        response = ec2.instances.filter(InstanceIds=instance_ids).terminate()
        print(response)

    def list(self):
        ec2_client = boto3.client('ec2')

    def filter(self, ids: list = []):
        ec2_client = boto3.resource('ec2')
        response = ec2_client.instances.filter(
            Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
        instances_list = []
        for instance in response:
            print(instance.instance_id)
            instances_list.append(instance.instance_id)
        return instances_list


class AWSRoute53():
    def __init__(self) -> None:
        super().__init__

    def create(self):
        # Create Route53 record for the API Gateway
        route53_client = boto3.client('route53')

        api_domain_name = 'api.example.com'  # Replace with your desired domain name
        route53_response = route53_client.change_resource_record_sets(
            HostedZoneId='your-hosted-zone-id',  # Replace with your Route53 hosted zone ID
            ChangeBatch={
                'Changes': [
                    {
                        'Action': 'CREATE',
                        'ResourceRecordSet': {
                            'Name': api_domain_name,
                            'Type': 'A',
                            'AliasTarget': {
                                'DNSName': f'{api_id}.execute-api.us-east-1.amazonaws.com',
                                'EvaluateTargetHealth': False
                            }
                        }
                    }
                ]
            }
        )


class EC2InstanceStack(Stack):

    def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        vpc = ec2.Vpc(self, "VPC",
                      nat_gateways=0,
                      subnet_configuration=[ec2.SubnetConfiguration(
                          name="public", subnet_type=ec2.SubnetType.PUBLIC)]
                      )

        amzn_linux = ec2.MachineImage.latest_amazon_linux(
            generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
            edition=ec2.AmazonLinuxEdition.STANDARD,
            virtualization=ec2.AmazonLinuxVirt.HVM,
            storage=ec2.AmazonLinuxStorage.GENERAL_PURPOSE
        )

        role = iam.Role(self, "InstanceSSM",
                        assumed_by=iam.ServicePrincipal("ec2.amazonaws.com"))

        role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name(
            "AmazonSSMManagedInstanceCore"))

        instance = ec2.Instance(self, "Instance",
                                instance_type=ec2.InstanceType("t3.nano"),
                                machine_image=amzn_linux,
                                vpc=vpc,
                                role=role
                                )

        # # Script in S3 as Asset
        # asset = Asset(self, "Asset", path=os.path.join(
        #     dirname, "configure.sh"))
        # local_path = instance.user_data.add_s3_download_command(
        #     bucket=asset.bucket,
        #     bucket_key=asset.s3_object_key
        # )

        # # Userdata executes script from S3
        # instance.user_data.add_execute_file_command(
        #     file_path=local_path
        # )
        # asset.grant_read(instance.role)


@jsii.implements(ICloudAssemblyDirectoryProducer)
class Producer:
    def produce(self, context):
        app = App(context=context)
        # stack = Stack(app)
        stack = EC2InstanceStack(app, "EC2Stack")
        return app.synth().directory
