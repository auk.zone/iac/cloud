import os
from aws_cdk import App
from aws_cdk.cli_lib_alpha import AwsCdkCli

import click
import google.cloud.compute_v1 as compute_v1
from auk.helpers.aws import AWSEC2


@click.group(invoke_without_command=False, help='Help to manage cloud platforms')
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def auk(ctx, debug):
    click.echo(f"Debug mode is {'on' if debug else 'off'}")


@auk.group(invoke_without_command=False, help='Helps you to manage Amazon Web Service Infrastructure')
@click.pass_context
def aws(ctx):
    pass


@aws.group(invoke_without_command=False, help='Helps you to manage your instances in Amazon Web Service Infrastructure')
@click.pass_context
def instance(ctx):
    pass


@instance.command(help='Create an instance')
@click.pass_context
def create(ctx):
    print("AWS create")


@instance.command(help='Destroy an instance')
@click.argument('all', nargs=-1)
@click.option('--instances-ids', type=list, default=[])
@click.pass_context
def destroy(ctx, all: bool = False, instances_ids: list = []):
    print("AWS destroy")
    ec2 = AWSEC2()
    if all:
        ec2.destroy(all=bool(all))


@instance.command(help='List instances')
@click.pass_context
def list(ctx):
    print("AWS list")


@instance.command(help='Filter instances')
@click.pass_context
def filter(ctx):
    print("AWS filter")
    ec2 = AWSEC2()
    ec2.filter()


@auk.group(invoke_without_command=False, help='Helps you to manage Google Cloud Infrastructure')
@click.pass_context
def google(ctx):
    pass


@google.group(invoke_without_command=False, help='Helps you to manage your instances in Google Cloud Infrastructure')
@click.pass_context
def instance(ctx):
    pass


@instance.command(help='Create an instance')
@click.pass_context
def create(ctx):
    print("Google create")


@instance.command(help='Destroy an instance')
@click.pass_context
def destroy(ctx):
    print("Google destroy")


@instance.command(help='List instances')
@click.pass_context
def list(ctx):
    print("Google list")


@instance.command(help='Filter instances')
@click.pass_context
def filter(ctx):
    print("Google filter")

# @aws.group(invoke_without_command=False)
# @aws.command('instance')
# @click.pass_context
# @click.argument('create', nargs=-1)
# @click.argument('list')
# def instance(ctx, create: bool = False, list: bool = False):
#     print('create instances')
#     if create:
#         ec2 = AWSEC2()
#         ec2.create()
#     if list:
#         ec2 = AWSEC2()
#         ec2.filter()


# @aws.command('instance')
# @click.pass_context
# @click.argument('destroy', nargs=-1)
# @click.option('--instance-id', type=str, default=None)
# def instance(ctx, instance_id: str = None):
#     print(f'destroying instances: {instance_id}')
#     ec2 = AWSEC2()
#     ec2.destroy(instance_id=instance_id)


# @aws.command('instance')
# @click.pass_context
# def instance(ctx):


# @google.command('instance')
# @click.pass_context
# def instance(ctx):
#     print('create instances')


# @google.command()
# @click.pass_context
# @click.option('--project')
# def print_images_list(ctx, project: str) -> str:
#     """
#     Prints a list of all non-deprecated image names available in given project.

#     Args:
#         project: project ID or project number of the Cloud project you want to list images from.

#     Returns:
#         The output as a string.
#     """
#     os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "AIzaSyB9caTZaHhJnm3rvX2kxEJzUtYQojn_3No"
#     images_client = compute_v1.ImagesClient()
#     # Listing only non-deprecated images to reduce the size of the reply.
#     images_list_request = compute_v1.ListImagesRequest(
#         project=project, max_results=100, filter="deprecated.state != DEPRECATED"
#     )
#     output = []

#     # Although the `max_results` parameter is specified in the request, the iterable returned
#     # by the `list()` method hides the pagination mechanic. The library makes multiple
#     # requests to the API for you, so you can simply iterate over all the images.
#     for img in images_client.list(request=images_list_request):
#         print(f" -  {img.name}")
#         output.append(f" -  {img.name}")
#     return "\n".join(output)
