import click
import requests


@click.group(invoke_without_command=False, help='Manage domains')
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def auk(ctx, debug):
    click.echo(f"Debug mode is {'on' if debug else 'off'}")


@auk.group(invoke_without_command=True, help='Helps you to manage CloudFlare domains')
@click.pass_context
def cloudflare(ctx):
    pass


@cloudflare.command('add_website')
@click.pass_context
def add_website_to_cloudflare(api_key, email, zone_name, domain_name):
    # Construct the API endpoint URL
    url = f"https://api.cloudflare.com/client/v4/zones"

    # Prepare the request headers
    headers = {
        "X-Auth-Email": email,
        "X-Auth-Key": api_key,
        "Content-Type": "application/json"
    }

    # Prepare the request payload
    payload = {
        "name": domain_name,
        "account": {
            "id": ""  # Enter your Cloudflare account ID here, if necessary
        },
        "jump_start": False
    }

    # Send the API request
    response = requests.post(url, headers=headers, json=payload)

    # Check the response status code
    if response.status_code == 200:
        data = response.json()
        zone_id = data["result"]["id"]
        print(
            f"Website '{domain_name}' added to Cloudflare with Zone ID: {zone_id}")
    else:
        print(
            f"Failed to add website '{domain_name}' to Cloudflare. Error: {response.text}")

    api_key = "YOUR_API_KEY"
    email = "YOUR_EMAIL"
    zone_name = "example.com"
    domain_name = "subdomain.example.com"

    add_website_to_cloudflare(api_key, email, zone_name, domain_name)
